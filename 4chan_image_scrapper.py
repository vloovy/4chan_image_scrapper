import re

import requests
import urllib.request

import sys
from bs4 import BeautifulSoup
import os


def thread_scanner(url):
    # Get Response object
    response_object = requests.get(url)
    if response_object.status_code == 404:
        print("Error 404")
        sys.exit(0)
    else:
        # Get html text from Response object
        html_text = response_object.text
        # Get BeautifulSoup (nested structure) object
        html_parsed_soup = BeautifulSoup(html_text, "html.parser")

        try:
            os.chdir(input("Please enter where you want to create folder for saved images: \n"))
            # invalid folder name because of forwardslash
            folder_name = get_folder_name(url)
            os.mkdir(folder_name)
            os.chdir(folder_name)
        except Exception as e:
            print(e)
            sys.exit(0)

        for link in html_parsed_soup.findAll("a", "fileThumb"):
            href = "https:"+link.get("href")
            print(href)
            # Download file using urllib
            urllib.request.urlretrieve(href, re.search(r"([0-9]+.[a-z]+)$", href).group())


def get_folder_name(url):
    invalid_folder_name = re.search(r"[a-z]+/thread/[0-9]+", url).group()
    valid_folder_name = re.sub(r"/", "_", invalid_folder_name)
    return valid_folder_name


def get_thread_url():
    url = input("Please enter threads url: \n")
    while re.search(r"https?://boards.4chan.org/[a-z]+/thread/[0-9]+", url) is None:
        url = input("Please enter valid url: \n")
    return url


def main():
    url = get_thread_url()
    thread_scanner(url)


if __name__ == "__main__":
    main()
